package be.hcpl.android.macremote.common;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import be.hcpl.android.macremote.legacy.model.MediaItem;

/**
 * Created by hanscappelle on 8/12/14.
 */
public class Constants {

    public static final String WEAR_COMMAND_APPLIST = "/applist/";

    public static final String WEAR_COMMAND_MESSAGE = "/message/";

    public static final String WEAR_COMMAND_APPNAME = "/appname/";

    public static final String WEAR_COMMAND_APPCOMMAND = "/appcommand/";

    /**
     * use this type for mediaItem types
     */
    public static final Type MEDIA_ITEM_TYPE = new TypeToken<List<MediaItem>>() {
    }.getType();

}
