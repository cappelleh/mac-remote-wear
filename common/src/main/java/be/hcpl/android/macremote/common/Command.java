package be.hcpl.android.macremote.common;

/**
 * Created by hanscappelle on 18/11/14.
 */
public enum Command {
    PREVIOUS,
    PLAY_PAUSE,
    NEXT,
    REWIND,
    FORWARD,
    MUTE,
    GET_VOLUME,
    SET_VOLUME,
    POWER,
    SLEEP,
    LIGHT_DOWN,
    LIGHT_UP,
    CURRENT_TRACK,
    SYSTEM_VOLUME;
}