package be.hcpl.android.macremote.legacy.event;

import be.hcpl.android.macremote.common.Command;

public class CommandResponseEvent {
    public Command command;
    public String message;

    public CommandResponseEvent(Command paramCommand, String paramString) {
        this.message = paramString;
        this.command = paramCommand;
    }
}