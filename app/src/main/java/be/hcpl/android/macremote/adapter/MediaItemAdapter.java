package be.hcpl.android.macremote.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import be.hcpl.android.macremote.R;
import be.hcpl.android.macremote.legacy.model.MediaItem;

/**
 * Created by hcpl on 10/12/14.
 */
public class MediaItemAdapter extends BaseAdapter {

    private List<MediaItem> items = new ArrayList<MediaItem>();

    private Context mContext;

    public MediaItemAdapter(Context context, List<MediaItem> items) {
        if( items != null )
            this.items = items;
        this.mContext = context;
    }

    public List<MediaItem> getItems() {
        return items;
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return items != null ? items.get(i) : null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        if (rowView == null) {
            rowView = LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_1, parent, false);
            final ViewHolder holder = new ViewHolder();
            holder.name = (TextView) rowView.findViewById(android.R.id.text1);
            // update text color here
            holder.name.setTextColor(mContext.getResources().getColor(R.color.primary_text));
            rowView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) rowView.getTag();
        final MediaItem item = items.get(position);

        holder.name.setText(item.getName());

        return rowView;

    }

    class ViewHolder {
        TextView name;
    }
}
