package be.hcpl.android.macremote.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import be.hcpl.android.appframework.TemplateFragment;
import be.hcpl.android.macremote.MainActivity;
import be.hcpl.android.macremote.R;
import be.hcpl.android.macremote.adapter.MediaItemAdapter;
import be.hcpl.android.macremote.legacy.manager.DatabaseManager;
import be.hcpl.android.macremote.legacy.model.MediaItem;


/**
 * Created by hanscappelle on 23/10/14.
 */
public class AppListFragment extends TemplateFragment {

    private DatabaseManager dbManager;

    private MediaItemAdapter adapter;

    /**
     * ctor
     * @return
     */
    public static AppListFragment createInstance() {
        return new AppListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // needed for action menu
        setHasOptionsMenu(true);

        // get the db manager
        dbManager = DatabaseManager.getInstance(getActivity().getApplicationContext());
    }

    @Override
    public boolean startsWithSeparator() {
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_applist, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListView listView = (ListView)view.findViewById(R.id.list);
        adapter = new MediaItemAdapter(getActivity().getApplicationContext(), null);
        listView.setAdapter(adapter);

        // listen for click events
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((MainActivity)getActivity()).switchContent(AppEditFragment.createInstance((MediaItem) adapter.getItem(position)));
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        List<MediaItem> items = dbManager.getAllMediaItems();

        adapter.getItems().clear();
        adapter.getItems().addAll(items);
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getTitleResourceId() {
        return R.string.title_manage_apps;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.list_apps, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_add:
                createApp();
                return true;
            case R.id.action_help :
                ((MainActivity)getActivity()).switchContent(AppHelpFragment.createInstance());
                return true;
        }
        return false;
    }

    private void createApp() {
        ((MainActivity)getActivity()).switchContent(AppEditFragment.createInstance());
    }

}
