package be.hcpl.android.macremote.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import be.hcpl.android.appframework.TemplateFragment;
import be.hcpl.android.macremote.MainActivity;
import be.hcpl.android.macremote.MainApplication;
import be.hcpl.android.macremote.R;
import be.hcpl.android.macremote.legacy.event.UpdateAppsEvent;
import be.hcpl.android.macremote.legacy.manager.DatabaseManager;
import be.hcpl.android.macremote.legacy.model.MediaItem;


/**
 * Created by hanscappelle on 23/10/14.
 */
public class AppEditFragment extends TemplateFragment {

    private static final String KEY_MEDIA_ITEM = "key_media_item";

    // currently selected item
    private MediaItem selectedItem;

    private DatabaseManager dbManager;

    // all views
    private EditText eName, ePlay, eNext, ePrev, eInfo;

    /**
     * ctor for editing an existing app
     *
     * @return
     */
    public static AppEditFragment createInstance(MediaItem mediaItem) {
        AppEditFragment f = new AppEditFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_MEDIA_ITEM, mediaItem);
        f.setArguments(bundle);
        return f;
    }

    /**
     * for creating new items
     *
     * @return
     */
    public static AppEditFragment createInstance() {
        return new AppEditFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get arguments here
        if (savedInstanceState != null)
            selectedItem = (MediaItem) savedInstanceState.getSerializable(KEY_MEDIA_ITEM);

        Bundle args = getArguments();
        if (args != null)
            selectedItem = (MediaItem) args.getSerializable(KEY_MEDIA_ITEM);

        // get the db manager
        dbManager = DatabaseManager.getInstance(getActivity().getApplicationContext());

        // needed to show action menu itels
        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_MEDIA_ITEM, selectedItem);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_app_edit, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // get all view here
        eName = (EditText) view.findViewById(R.id.edit_name);
        ePlay = (EditText) view.findViewById(R.id.edit_play_pause);
        eNext = (EditText) view.findViewById(R.id.edit_next);
        ePrev = (EditText) view.findViewById(R.id.edit_previous);
        eInfo = (EditText) view.findViewById(R.id.edit_info);
    }

    @Override
    public void onResume() {
        super.onResume();

        // populate all fields here
        if (selectedItem != null) {
            eName.setText(selectedItem.getName());
            ePlay.setText(selectedItem.getPlayPause());
            eNext.setText(selectedItem.getNext());
            ePrev.setText(selectedItem.getPrevious());
            eInfo.setText(selectedItem.getInfo());

            // also update title
            getActivity().setTitle(new StringBuilder(getString(R.string.title_edit_app)).append(" : ").append(selectedItem.getName()));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit_app, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_save:
                updateApp();
                return true;
            case R.id.action_delete:
                deleteApp();
                return true;
        }
        return false;
    }

    private void deleteApp() {
        if( selectedItem == null ){
            // we can just navigate back, nothing created anyway
            getActivity().onBackPressed();
        }

        // otherwise confirm first
        else {
            //Ask the user if they want to quit
            new AlertDialog.Builder(getActivity())
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    //.setTitle(R.string.action_delete)
                    .setMessage(R.string.confirm_delete)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            // delete
                            dbManager.deleteMediaItem(selectedItem);
                            // post event
                            ((MainApplication)getActivity().getApplication()).getBus().post(new UpdateAppsEvent());
                            // and return
                            getActivity().onBackPressed();

                        }

                    })
                    .setNegativeButton(R.string.no, null)
                    .show();
        }
    }

    private void updateApp() {

        // input validation
        if( eName.getText().toString().length() < 2 ){
            Toast.makeText(getActivity(), R.string.msg_input_incomplete, Toast.LENGTH_SHORT).show();
            return;
        }

        // create new app if needed
        boolean newItem = false;
        if( selectedItem == null ){
            selectedItem = new MediaItem();
            newItem = true;
        }

        // save all input here
        selectedItem.setName(eName.getText().toString());
        selectedItem.setPlayPause(ePlay.getText().toString());
        selectedItem.setPrevious(ePrev.getText().toString());
        selectedItem.setNext(eNext.getText().toString());
        selectedItem.setInfo(eInfo.getText().toString());

        if( newItem){
            dbManager.addMediaItem(selectedItem);
        } else {
            // and update the item
            dbManager.updateMediaItem(selectedItem);
        }

        // if this was the current media item we need to update that one so the events are handled
        // correctly when commands enter from the wearable
        // needed for update of menu
        ((MainApplication)getActivity().getApplication()).getBus().post(new UpdateAppsEvent());

        // and navigate back to overview
        ((MainActivity) getActivity()).switchContent(AppListFragment.createInstance());
    }


    @Override
    public int getTitleResourceId() {
        return R.string.title_edit_app;
    }

}
