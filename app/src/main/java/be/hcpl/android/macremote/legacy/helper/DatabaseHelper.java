package be.hcpl.android.macremote.legacy.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import be.hcpl.android.macremote.BuildConfig;
import be.hcpl.android.macremote.legacy.model.AppData;
import be.hcpl.android.macremote.legacy.model.Credentials;
import be.hcpl.android.macremote.legacy.model.MediaItem;
import be.hcpl.android.macremote.legacy.model.SystemItem;
import java.util.ArrayList;
import java.util.Iterator;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper
{
  private static final String DATABASE_NAME = BuildConfig.APPLICATION_ID+".sqlite";
  private static final int DATABASE_VERSION = 1;
  private Dao<AppData, Integer> appDataDao = null;
  private Dao<Credentials, Integer> credentialsDao = null;
  private Dao<MediaItem, Integer> mediaItemDao = null;
  private Dao<SystemItem, Integer> systemItemDao = null;

  public DatabaseHelper(Context paramContext)
  {
    super(paramContext, DATABASE_NAME, null, DATABASE_VERSION);
  }

  public Dao<AppData, Integer> getAppDataDao()
  {
    if (this.appDataDao == null);
    try
    {
      this.appDataDao = getDao(AppData.class);
      return this.appDataDao;
    }
    catch (java.sql.SQLException localSQLException)
    {
      while (true)
        localSQLException.printStackTrace();
    }
  }

  public Dao<Credentials, Integer> getCredentialsDao()
  {
    if (this.credentialsDao == null);
    try
    {
      this.credentialsDao = getDao(Credentials.class);
      return this.credentialsDao;
    }
    catch (java.sql.SQLException localSQLException)
    {
      while (true)
        localSQLException.printStackTrace();
    }
  }

  public Dao<MediaItem, Integer> getMediaItemDao()
  {
    if (this.mediaItemDao == null);
    try
    {
      this.mediaItemDao = getDao(MediaItem.class);
      return this.mediaItemDao;
    }
    catch (java.sql.SQLException localSQLException)
    {
      while (true)
        localSQLException.printStackTrace();
    }
  }

  public Dao<SystemItem, Integer> getSystemItemDao()
  {
    if (this.systemItemDao == null);
    try
    {
      this.systemItemDao = getDao(SystemItem.class);
      return this.systemItemDao;
    }
    catch (java.sql.SQLException localSQLException)
    {
      while (true)
        localSQLException.printStackTrace();
    }
  }

  public void onCreate(SQLiteDatabase paramSQLiteDatabase, ConnectionSource paramConnectionSource)
  {
    try
    {
      TableUtils.createTable(paramConnectionSource, SystemItem.class);
      TableUtils.createTable(paramConnectionSource, MediaItem.class);
      TableUtils.createTable(paramConnectionSource, Credentials.class);
      TableUtils.createTable(paramConnectionSource, AppData.class);
      return;
    }
    catch (android.database.SQLException localSQLException1)
    {
      Log.e(DatabaseHelper.class.getName(), "Can't create database", localSQLException1);
      throw new RuntimeException(localSQLException1);
    }
    catch (java.sql.SQLException localSQLException)
    {
      localSQLException.printStackTrace();
    }
  }

  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, ConnectionSource paramConnectionSource, int paramInt1, int paramInt2)
  {
    try
    {
      Iterator localIterator = new ArrayList().iterator();
      while (localIterator.hasNext())
        paramSQLiteDatabase.execSQL((String)localIterator.next());
    }
    catch (android.database.SQLException localSQLException)
    {
      Log.e(DatabaseHelper.class.getName(), "exception during onUpgrade", localSQLException);
      throw new RuntimeException(localSQLException);
    }
  }
}