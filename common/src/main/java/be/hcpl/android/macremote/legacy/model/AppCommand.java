package be.hcpl.android.macremote.legacy.model;

import java.io.Serializable;

import be.hcpl.android.macremote.common.Command;

/**
 * Created by hcpl on 3/04/15.
 */
public class AppCommand implements Serializable {

    private String appName;

    private Command appCommand;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Command getAppCommand() {
        return appCommand;
    }

    public void setAppCommand(Command appCommand) {
        this.appCommand = appCommand;
    }
}
