package be.hcpl.android.macremote.legacy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import be.hcpl.android.appframework.TemplateFragment;
import be.hcpl.android.macremote.MainActivity;
import be.hcpl.android.macremote.MainApplication;
import be.hcpl.android.macremote.R;
import be.hcpl.android.macremote.fragment.AboutFragment;
import be.hcpl.android.macremote.legacy.event.ConnectionFailureEvent;
import be.hcpl.android.macremote.legacy.event.ConnectionSuccesfulEvent;
import be.hcpl.android.macremote.legacy.manager.ConnectionManager;
import be.hcpl.android.macremote.legacy.manager.DatabaseManager;
import be.hcpl.android.macremote.legacy.model.Credentials;
import be.hcpl.android.macremote.legacy.model.MediaItem;

/**
 * enter credentials here to connect to your mac
 */
public class ConnectFragment extends TemplateFragment {

    private static final String KEY_MEDIA_ITEM = "calling_fragment_media_item";

    // private View connectView;
    // private TextView helpButton;

    private ConnectionManager connectionManager;
    private DatabaseManager dbManager;

    private AutoCompleteTextView hostAutocomplete;
    private AutoCompleteTextView usernameAutoComplete;
    private AutoCompleteTextView passwordAutoComplete;

    // FIXME doesn't work, got duplicates + has to move to activity instead
//    private boolean isFirstRun;

    // the media item we're working on
    private MediaItem currentMediaItem = null;

    // TODO implement progress view during connection
    // TODO feed autocompletes here
    // TODO implement help section
/*
    private TextWatcher textWatcher = new TextWatcher() {
        public void afterTextChanged(Editable paramAnonymousEditable) {
            ConnectFragment.this.enableDisableConnectButton();
        }

        public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {
        }

        public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {
        }
    };
    */

    /**
     * ctor if executed from nothing, results in about screen if done
     *
     * @return
     */
    public static ConnectFragment createInstance() {
        return new ConnectFragment();
    }

    /**
     * alternative ctor takes the fragment that started this connect request
     *
     * @return
     */
    public static ConnectFragment createInstance(MediaItem item) {
        ConnectFragment f = new ConnectFragment();
        Bundle b = new Bundle();
        b.putSerializable(KEY_MEDIA_ITEM, item);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_MEDIA_ITEM, currentMediaItem);
    }

    @Override
    public int getTitleResourceId() {
        return R.string.title_connect;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.connect, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_connect:
                connect();
                return true;
        }
        return false;
    }

    /**
     * helper to connect over ssh with mac
     */
    private void connect() {
        ConnectFragment.this.startProgress();
        Credentials localCredentials = new Credentials();
        localCredentials.setUsername(ConnectFragment.this.usernameAutoComplete.getText().toString());
        localCredentials.setPassword(ConnectFragment.this.passwordAutoComplete.getText().toString());
        localCredentials.setIp(ConnectFragment.this.hostAutocomplete.getText().toString());
        ConnectFragment.this.connectionManager.connect(localCredentials);
    }

    /*
        private void enableDisableConnectButton() {
            if ((this.usernameAutoComplete.length() != 0) && (this.passwordAutoComplete.length() != 0) && (this.hostAutocomplete.length() != 0))
                ;
            for (boolean bool = true; ; bool = false) {
                this.connectView.setEnabled(bool);
                //this.progressImageView.setEnabled(bool);
                //this.connectViewTextView.setEnabled(bool);
                return;
            }
        }
    */
    private void initAutocomplete() {

        Credentials localCredentials = this.dbManager.getAppData().getCurrentCredentials();
        if (localCredentials == null)
            localCredentials = new Credentials();
        this.usernameAutoComplete.setText(localCredentials.getUsername());
        String str = localCredentials.getUsername();
        int i = 0;
        if (str != null)
            i = localCredentials.getUsername().length();
        this.usernameAutoComplete.setSelection(i);
        this.passwordAutoComplete.setText(localCredentials.getPassword());
        this.hostAutocomplete.setText(localCredentials.getIp());
        //enableDisableConnectButton();
        //this.usernameAutoComplete.addTextChangedListener(this.textWatcher);
        //this.passwordAutoComplete.addTextChangedListener(this.textWatcher);
        //this.hostAutocomplete.addTextChangedListener(this.textWatcher);
        // DONE fixed white text on adapter
        ArrayAdapter localArrayAdapter1 = new ArrayAdapter(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, this.dbManager.getAllUsernames()){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                TextView text = (TextView) v.findViewById(android.R.id.text1);
                // update text color here
                text.setTextColor(getResources().getColor(R.color.primary_text));
                return v;
            }
        };
        this.usernameAutoComplete.setAdapter(localArrayAdapter1);
        ArrayAdapter localArrayAdapter2 = new ArrayAdapter(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, this.dbManager.getAllIps()){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                TextView text = (TextView) v.findViewById(android.R.id.text1);
                // update text color here
                text.setTextColor(getResources().getColor(R.color.primary_text));
                return v;
            }
        };
        this.hostAutocomplete.setAdapter(localArrayAdapter2);
    }

    private void startProgress() {
        this.usernameAutoComplete.setEnabled(false);
        this.passwordAutoComplete.setEnabled(false);
        this.hostAutocomplete.setEnabled(false);
        //this.helpButton.setEnabled(false);
        //this.connectView.setClickable(false);
        //this.connectView.setSelected(true);
        //this.connectViewTextView.setVisibility(4);
    }

    private void stopProgress() {
        this.usernameAutoComplete.setEnabled(true);
        this.passwordAutoComplete.setEnabled(true);
        this.hostAutocomplete.setEnabled(true);
        //this.helpButton.setEnabled(true);
        //this.connectView.setClickable(true);
        //this.connectView.setSelected(false);
        //this.connectViewTextView.setVisibility(0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_connect, container, false);
    }

    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);

        // needed for action menu
        setHasOptionsMenu(true);

        // restore the state
        Bundle args = getArguments();
        if (args != null && args.containsKey(KEY_MEDIA_ITEM)) {
            currentMediaItem = (MediaItem) args.getSerializable(KEY_MEDIA_ITEM);
        } else if (paramBundle != null && paramBundle.containsKey(KEY_MEDIA_ITEM))
            currentMediaItem = (MediaItem) paramBundle.getSerializable(KEY_MEDIA_ITEM);

        this.dbManager = DatabaseManager.getInstance(getActivity().getApplicationContext());
        this.connectionManager = ConnectionManager.getInstance((MainApplication) getActivity().getApplication());

        // check connection
        if (this.connectionManager.isConnected())
            onEvent(new ConnectionSuccesfulEvent());

        // register for events
        ((MainApplication) getActivity().getApplication()).getBus().register(this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // get all view references
        this.usernameAutoComplete = ((AutoCompleteTextView) view.findViewById(R.id.text_username));
        this.passwordAutoComplete = ((AutoCompleteTextView) view.findViewById(R.id.text_password));
        this.hostAutocomplete = ((AutoCompleteTextView) view.findViewById(R.id.text_host));

        initAutocomplete();
    }

    public void onDestroy() {
        super.onDestroy();
        ((MainApplication) getActivity().getApplication()).getBus().unregister(this);
    }

    @Subscribe
    public void onEvent(ConnectionFailureEvent paramConnectionFailureEvent) {
        // err msg
        Toast.makeText(getActivity().getApplicationContext(), paramConnectionFailureEvent.message, Toast.LENGTH_LONG).show();
        stopProgress();
    }

    @Subscribe
    public void onEvent(ConnectionSuccesfulEvent paramConnectionSuccesfulEvent) {
        if (currentMediaItem != null)
            ((MainActivity) getActivity()).switchContent(ControlsFragment.createInstance(currentMediaItem));
        else
            ((MainActivity) getActivity()).switchContent(AboutFragment.createInstance());
    }

    // cycle in back issue resolved by nothing adding this fragment to backstack, ever
    // counter back here to avoid the loop, this connect fragment is only used in between for
    // creating the connection if control fragment found it was not connected so it's safe to
    // go back 2x if back is requested

    // we could've made an onBackPressed here but instead handled all in the MainActivity since
    // only needed for one fragment
}