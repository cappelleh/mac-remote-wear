package be.hcpl.android.macremote.legacy.event;

public class ConnectionFailureEvent
{
  public String message;

  public ConnectionFailureEvent(String paramString)
  {
    this.message = paramString;
  }
}