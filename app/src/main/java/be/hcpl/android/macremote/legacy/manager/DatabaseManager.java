package be.hcpl.android.macremote.legacy.manager;

import android.content.Context;

import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import be.hcpl.android.macremote.legacy.helper.DatabaseHelper;
import be.hcpl.android.macremote.legacy.model.AppData;
import be.hcpl.android.macremote.legacy.model.Credentials;
import be.hcpl.android.macremote.legacy.model.MediaItem;
import be.hcpl.android.macremote.legacy.model.SystemItem;

public class DatabaseManager {
    private static DatabaseManager instance;
    private DatabaseHelper helper;

    private DatabaseManager(Context paramContext) {
        this.helper = new DatabaseHelper(paramContext);
    }

    private DatabaseHelper getHelper() {
        return this.helper;
    }

    private Credentials getIdenticalItem(Credentials paramCredentials) {
        try {
            QueryBuilder localQueryBuilder = getHelper().getCredentialsDao().queryBuilder();
            localQueryBuilder.where().eq("username", paramCredentials.getUsername()).and().eq("password", paramCredentials.getPassword()).and().eq("ip", paramCredentials.getIp());
            Credentials localCredentials = (Credentials) localQueryBuilder.queryForFirst();
            return localCredentials;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
        return null;
    }

    public static DatabaseManager getInstance(Context paramContext) {
        if (instance == null)
            instance = new DatabaseManager(paramContext);
        return instance;
    }

    private List<MediaItem> getRemovedMediaItems(boolean paramBoolean) {
        try {
            QueryBuilder localQueryBuilder = getHelper().getMediaItemDao().queryBuilder();
            localQueryBuilder.where().eq("is_removed", Boolean.valueOf(paramBoolean));
            localQueryBuilder.orderBy("position", true);
            List localList = localQueryBuilder.query();
            return localList;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
        return null;
    }

    public void addAppData(AppData paramAppData) {
        try {
            getHelper().getAppDataDao().create(paramAppData);
            return;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
    }

    public void addCredentials(Credentials paramCredentials) {
        try {
            getHelper().getCredentialsDao().create(paramCredentials);
            return;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
    }

    public Credentials addDistinctCredentials(Credentials paramCredentials) {
        Credentials localCredentials = getIdenticalItem(paramCredentials);
        if (localCredentials == null) {
            addCredentials(paramCredentials);
            return paramCredentials;
        }
        return localCredentials;
    }

    public void addMediaItem(MediaItem paramMediaItem) {
        try {
            getHelper().getMediaItemDao().create(paramMediaItem);
            return;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
    }

    public void addMediaItems(List<MediaItem> paramList) {
        int i = 0;
        try {
            while (i < paramList.size()) {
                MediaItem localMediaItem = (MediaItem) paramList.get(i);
                localMediaItem.setPosition(i);
                getHelper().getMediaItemDao().create(localMediaItem);
                i++;
            }
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
    }

    public void addSystemItem(SystemItem paramSystemItem) {
        try {
            getHelper().getSystemItemDao().create(paramSystemItem);
            return;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
    }

    public void addSystemItems(List<SystemItem> paramList) {
        int i = 0;
        try {
            while (i < paramList.size()) {
                SystemItem localSystemItem = (SystemItem) paramList.get(i);
                localSystemItem.setPosition(i);
                getHelper().getSystemItemDao().create(localSystemItem);
                i++;
            }
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
    }

    public void createOrUpdateMediaItem(MediaItem paramMediaItem) {
        try {
            getHelper().getMediaItemDao().createOrUpdate(paramMediaItem);
            return;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
    }

    public void deleteMediaItem(MediaItem paramMediaItem) {
        try {
            getHelper().getMediaItemDao().delete(paramMediaItem);
            return;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
    }

    public void deleteSystemItem(SystemItem paramSystemItem) {
        try {
            getHelper().getSystemItemDao().delete(paramSystemItem);
            return;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
    }

    public List<Credentials> getAllCredentials() {
        try {
            List localList = getHelper().getCredentialsDao().queryForAll();
            return localList;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
        return null;
    }

    public List<String> getAllIps() {
        ArrayList localArrayList2 = new ArrayList();
        try {
            QueryBuilder localQueryBuilder = getHelper().getCredentialsDao().queryBuilder();
            localQueryBuilder.selectColumns(new String[]{"ip"});
            List localList = localQueryBuilder.query();
            Iterator localIterator = localList.iterator();
            while (localIterator.hasNext())
                localArrayList2.add(((Credentials) localIterator.next()).getIp());
        } catch (SQLException localSQLException1) {
            localSQLException1.printStackTrace();
        }
        return localArrayList2;
    }

    public List<MediaItem> getAllMediaItems() {
        try {
            List localList = getHelper().getMediaItemDao().queryForAll();
            return localList;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
        return null;
    }

    public List<SystemItem> getAllSystemItems() {
        try {
            List localList = getHelper().getSystemItemDao().queryForAll();
            return localList;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
        return null;
    }

    public List<String> getAllUsernames() {
        ArrayList localArrayList2 = new ArrayList();
        try {
            QueryBuilder localQueryBuilder = getHelper().getCredentialsDao().queryBuilder();
            localQueryBuilder.selectColumns(new String[]{"username"});
            List localList = localQueryBuilder.query();
            Iterator localIterator = localList.iterator();
            while (localIterator.hasNext())
                localArrayList2.add(((Credentials) localIterator.next()).getUsername());
        } catch (SQLException localSQLException1) {
            localSQLException1.printStackTrace();
        }
        return localArrayList2;
    }

    public AppData getAppData() {
        try {
            AppData localAppData = (AppData) getHelper().getAppDataDao().queryForAll().get(0);
            return localAppData;
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return null;
    }

    public MediaItem getMediaItemWithId(int paramInt) {
        try {
            MediaItem localMediaItem = (MediaItem) getHelper().getMediaItemDao().queryForId(Integer.valueOf(paramInt));
            return localMediaItem;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
        return null;
    }

    public MediaItem getMediaItemByName(String name) {
        try {
            List<MediaItem> items = (List<MediaItem>) getHelper().getMediaItemDao().queryForEq("name", name);
            if( !items.isEmpty())
                return items.get(0);
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
        return null;
    }

    public MediaItem getNotRemovedMediaItemWithPosition(int paramInt) {
        try {
            QueryBuilder localQueryBuilder = getHelper().getMediaItemDao().queryBuilder();
            localQueryBuilder.where().eq("position", Integer.valueOf(paramInt)).and().eq("is_removed", Boolean.valueOf(false));
            localQueryBuilder.orderBy("position", true);
            MediaItem localMediaItem = (MediaItem) localQueryBuilder.queryForFirst();
            return localMediaItem;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
        return null;
    }

    public List<MediaItem> getNotRemovedMediaItems() {
        return getRemovedMediaItems(false);
    }

    public List<String> getNotRemovedMediaItemsNames() {
        List localList = getNotRemovedMediaItems();
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = localList.iterator();
        while (localIterator.hasNext())
            localArrayList.add(((MediaItem) localIterator.next()).getName());
        return localArrayList;
    }

    public List<MediaItem> getRemovedMediaItems() {
        return getRemovedMediaItems(true);
    }

    public SystemItem getSystemItemWithId(int paramInt) {
        try {
            SystemItem localSystemItem = (SystemItem) getHelper().getSystemItemDao().queryForId(Integer.valueOf(paramInt));
            return localSystemItem;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
        return null;
    }

    public SystemItem getSystemItemWithName(String paramString) {
        try {
            SystemItem localSystemItem = (SystemItem) getHelper().getSystemItemDao().queryForEq("name", paramString).get(0);
            return localSystemItem;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
        return null;
    }

    public MediaItem newMediaItem() {
        MediaItem localMediaItem = new MediaItem();
        try {
            getHelper().getMediaItemDao().create(localMediaItem);
            return localMediaItem;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
        return localMediaItem;
    }

    public void refreshSystemItem(SystemItem paramSystemItem) {
        try {
            getHelper().getSystemItemDao().refresh(paramSystemItem);
            return;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
    }

    public void updateAppData(AppData paramAppData) {
        try {
            getHelper().getAppDataDao().update(paramAppData);
            return;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
    }

    public void updateMediaItem(MediaItem paramMediaItem) {
        try {
            getHelper().getMediaItemDao().update(paramMediaItem);
            return;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
    }

    public void updateSystemItem(SystemItem paramSystemItem) {
        try {
            getHelper().getSystemItemDao().update(paramSystemItem);
            return;
        } catch (SQLException localSQLException) {
            localSQLException.printStackTrace();
        }
    }
}