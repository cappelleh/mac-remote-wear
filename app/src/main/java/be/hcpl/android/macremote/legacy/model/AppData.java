package be.hcpl.android.macremote.legacy.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class AppData
{
  public static final String CURRENT_CREDENTIALS = "current_credentials";
  public static final String CURRENT_MEDIA_ITEM = "current_media_item";
  public static final String FIRST_RUN_TIMESTAMP = "first_run_timestamp";
  public static final String ID = "id";
  public static final String IS_DRAWER_SHOWN = "is_drawer_shown";

  @DatabaseField(columnName="current_credentials", foreign=true, foreignAutoRefresh=true)
  private Credentials currentCredentials;

  @DatabaseField(columnName="current_media_item", foreign=true, foreignAutoRefresh=true)
  private MediaItem currentMediaItem;

  @DatabaseField(columnName="first_run_timestamp")
  private String firstRunTimestamp;

  @DatabaseField(columnName="id", generatedId=true)
  private int id;

  @DatabaseField(columnName="is_ad_remove_dialog_shown", defaultValue="false")
  private boolean isAdRemoveDialogShown;

  @DatabaseField(columnName="is_drawer_shown", defaultValue="false")
  private boolean isDrawerShown;

  public Credentials getCurrentCredentials()
  {
    return this.currentCredentials;
  }

  public MediaItem getCurrentMediaItem()
  {
    return this.currentMediaItem;
  }

  public String getFirstRunTimestamp()
  {
    return this.firstRunTimestamp;
  }

  public int getId()
  {
    return this.id;
  }

  public boolean isAdRemoveDialogShown()
  {
    return this.isAdRemoveDialogShown;
  }

  public boolean isDrawerShown()
  {
    return this.isDrawerShown;
  }

  public void setAdRemoveDialogShown(boolean paramBoolean)
  {
    this.isAdRemoveDialogShown = paramBoolean;
  }

  public void setCurrentCredentials(Credentials paramCredentials)
  {
    this.currentCredentials = paramCredentials;
  }

  public void setCurrentMediaItem(MediaItem paramMediaItem)
  {
    this.currentMediaItem = paramMediaItem;
  }

  public void setDrawerShown(boolean paramBoolean)
  {
    this.isDrawerShown = paramBoolean;
  }

  public void setFirstRunTimestamp(String paramString)
  {
    this.firstRunTimestamp = paramString;
  }

  public void setId(int paramInt)
  {
    this.id = paramInt;
  }
}