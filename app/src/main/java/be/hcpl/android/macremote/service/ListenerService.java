package be.hcpl.android.macremote.service;

import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import java.util.List;
import java.util.concurrent.TimeUnit;

import be.hcpl.android.macremote.MainApplication;
import be.hcpl.android.macremote.R;
import be.hcpl.android.macremote.common.Command;
import be.hcpl.android.macremote.common.Constants;
import be.hcpl.android.macremote.legacy.event.CommandResponseEvent;
import be.hcpl.android.macremote.legacy.manager.ConnectionManager;
import be.hcpl.android.macremote.legacy.manager.DatabaseManager;
import be.hcpl.android.macremote.legacy.manager.JsonManager;
import be.hcpl.android.macremote.legacy.model.AppCommand;
import be.hcpl.android.macremote.legacy.model.AppData;
import be.hcpl.android.macremote.legacy.model.MediaItem;

/**
 * Created by hanscappelle on 18/11/14.
 */
public class ListenerService extends WearableListenerService {

    private static final long MSG_CONNECTION_TIME_OUT_MS = 1000;

    // disconnect form server every minute to safe battery
    private static final long SSH_CONNECTION_TIME_OUT_MS = 60 * 1000;

    /**
     * nodeId is stored for the reply
     */
    private String nodeId;

    private ConnectionManager connectionManager;

    private DatabaseManager dbManager;

    //private static final int NOTIFICATION_ID = 001;

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        nodeId = messageEvent.getSourceNodeId();
        handleIncomingMessage(messageEvent.getPath());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // register for events
        ((MainApplication) getApplication()).getBus().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // release connection
        // TODO nothing to release db here? if( dbManager != null && dbManager.clo)
        if (connectionManager != null && connectionManager.isConnected())
            connectionManager.disconnect();
        // unregister for events
        ((MainApplication) getApplication()).getBus().unregister(this);
    }

    /**
     * helper for toast creation for feedback to user on handheld
     *
     * @param message
     */
    private void handleIncomingMessage(String message) {

        // if no message just ignore
        if (message == null)
            return;

        // check if we need to init data here, this will be our standard response no matter what
        // actual content of the message was (not return statements, this even blocks)
        dbManager = DatabaseManager.getInstance(getApplicationContext());
        AppData appData = dbManager.getAppData();
        if (appData == null) {
            reply(Constants.WEAR_COMMAND_MESSAGE + getString(R.string.err_app_not_configured));
            // do we just make a notification that will popup on connected wear anyway?
            return;
        }

        // check if we have a user
        if (appData.getCurrentCredentials() == null) {
            reply(Constants.WEAR_COMMAND_MESSAGE + getString(R.string.err_no_user_configured));
            return;
        }

        // send back a complete list of all available apps in this config
        if( message.startsWith(Constants.WEAR_COMMAND_APPLIST)) {
            // retrieve all apps
            List<MediaItem> mediaItems = dbManager.getAllMediaItems();
            // FIXED index based to error sensitive
//            if (mediaItems != null && !mediaItems.isEmpty()) {
//                StringBuilder result = new StringBuilder(Constants.KEY_APPLIST);
//                for (MediaItem mi : mediaItems) {
//                    result.append(mi.getName());
//                    result.append(";");
//                }
//                // clean up
//                String msg = result.length() > 0 ? result.substring(0, result.length() - 1) : "";
//                // if it were all strings use guava
//                // Joiner.on(",").join(collectionOfStrings);
//                reply(Constants.WEAR_COMMAND_APPLIST + msg);
//            }
            // WHOAHAHAHA don't go from string preference to obj and then back to json !!??
            Gson gson = new Gson();
            String jsonMessage = gson.toJson(mediaItems, Constants.MEDIA_ITEM_TYPE);
            reply(Constants.WEAR_COMMAND_APPLIST+jsonMessage);
            return;
        }

//        // empty messages stop here, they are just for syncing
//        if (message.equals("")) {
//            // stops here
//            return;
//        }

//        Toast.makeText(this, new StringBuilder().append(getString(R.string.msg_received)).append(": ").append(message), Toast.LENGTH_SHORT).show();

        // TODO see if we have a connection first, if note we can respond with failure

        AppCommand appCommand = null;

        // check if there is an app name in the message (optional param)
//        int indexClosing;
//        if (message != null && message.indexOf(Constants.KEY_OPEN_APP) == 0 && (indexClosing = message.indexOf(Constants.KEY_CLOSE_APP)) > -1) {
        if( message != null && message.startsWith(Constants.WEAR_COMMAND_APPCOMMAND)){
//
//            // get that app name
//            String appName = message.substring(1, indexClosing);//.replace(Constants.KEY_OPEN_APP, "");
            appCommand = new Gson().fromJson(message.replace(Constants.WEAR_COMMAND_APPCOMMAND, ""), AppCommand.class);
            // and try to load that app config
            MediaItem item = dbManager.getMediaItemByName(appCommand.getAppName());
            if (item != null) {
                appData.setCurrentMediaItem(item);
                // and save this app data here
                dbManager.updateAppData(appData);
            }

//            // remove the app name from the message
//            message = message.substring(indexClosing + 1);
        }

        // and app needs to be defined or configured
        if (appData.getCurrentMediaItem() == null) {
            reply(Constants.WEAR_COMMAND_MESSAGE + getString(R.string.err_no_app_selected));
            return;
        }

        // otherwise reply with current app data
        reply(Constants.WEAR_COMMAND_APPNAME + appData.getCurrentMediaItem().getName());

        // if db and app configured we should be good to go
        // connect
        connectionManager = ConnectionManager.getInstance((MainApplication) getApplication());
        // create a new connection if needed
        if (!connectionManager.isConnected()) {
            connectionManager.connect(appData.getCurrentCredentials());
            // here we should also create a connect
            new CountDownTimer(SSH_CONNECTION_TIME_OUT_MS, 1000) {

                @Override
                public void onFinish() {
                    //some script here
                    if (connectionManager != null && connectionManager.isConnected())
                        connectionManager.disconnect();
                }

                @Override
                public void onTick(long millisUntilFinished) {
                    //some script here
                }
            }.start();
        }
        // if still connected no need to connect again
        else {
            // nothing to do
        }

        // and send command for processing
        if( appCommand != null )
        connectionManager.execute(appCommand.getAppCommand());
    }

    /**
     * helper for feedback on wearable
     *
     * @param message
     */
    private void reply(final String message) {

        // information only
        Log.d(ListenerService.class.getSimpleName(), "Sending message reply: " + message);

        // perform in background (needed for event handling from bus)
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                // send a reply over the same communication channel
                GoogleApiClient client = new GoogleApiClient.Builder(getApplicationContext())
                        .addApi(Wearable.API)
                        .build();
                client.blockingConnect(MSG_CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                Wearable.MessageApi.sendMessage(client, nodeId, message, null);
                client.disconnect();
            }
        });
        thread.start();

    }

    @Subscribe
    public void onEvent(CommandResponseEvent paramCommandResponseEvent) {
        // all command responses are retrieved here
        String str = paramCommandResponseEvent.message.replace("\n", "");
        switch (paramCommandResponseEvent.command) {
            default:
                return;
            case CURRENT_TRACK:
                // show the current track in app here
                Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                // communicate it back to the wear app also
                reply(Constants.WEAR_COMMAND_MESSAGE + str);

        }
    }

}