package be.hcpl.android.macremote.legacy.manager;

import android.content.Context;

import be.hcpl.android.macremote.common.Command;
import be.hcpl.android.macremote.legacy.model.MediaItem;
import be.hcpl.android.macremote.legacy.model.SystemItem;

public class CommandManager {
    private static CommandManager instance;
    public final String GET_VOLUME_SYSTEM_ITEM_NAME = "get_volume";
    public final String LIGHT_DOWN_SYSTEM_ITEM_NAME = "light_down";
    public final String LIGHT_UP_SYSTEM_ITEM_NAME = "light_up";
    public final String MUTE_SYSTEM_ITEM_NAME = "mute";
    public final String POWER_SYSTEM_ITEM_NAME = "power";
    public final String SET_VOLUME_SYSTEM_ITEM_NAME = "set_volume";
    public final String SLEEP_SYSTEM_ITEM_NAME = "sleep";
    private Context ctx;

    private CommandManager(Context paramContext) {
        this.ctx = paramContext;
    }

    private String getAppleScriptCommand(Command paramCommand) {
        MediaItem localMediaItem = DatabaseManager.getInstance(this.ctx).getAppData().getCurrentMediaItem();
        switch (paramCommand) {
            default:
                return "";
            case PREVIOUS:
                return localMediaItem.getPrevious();
            case PLAY_PAUSE:
                return localMediaItem.getPlayPause();
            case NEXT:
                return localMediaItem.getNext();
            case REWIND:
                return localMediaItem.getRewind();
            case FORWARD:
                return localMediaItem.getFastForward();
            case CURRENT_TRACK:
                return localMediaItem.getInfo();
            case MUTE:
                return getSystemItem(MUTE_SYSTEM_ITEM_NAME).getCommand();
            case GET_VOLUME:
                return getSystemItem(GET_VOLUME_SYSTEM_ITEM_NAME).getCommand();
            case POWER:
                return getSystemItem(POWER_SYSTEM_ITEM_NAME).getCommand();
            case SLEEP:
                return getSystemItem(SLEEP_SYSTEM_ITEM_NAME).getCommand();
            case LIGHT_DOWN:
                return getSystemItem(LIGHT_DOWN_SYSTEM_ITEM_NAME).getCommand();
            case LIGHT_UP:
                return getSystemItem(LIGHT_UP_SYSTEM_ITEM_NAME).getCommand();
        }
    }

    public static CommandManager getInstance(Context paramContext) {
        if (instance == null)
            instance = new CommandManager(paramContext);
        return instance;
    }

    private SystemItem getSystemItem(String paramString) {
        return DatabaseManager.getInstance(this.ctx).getSystemItemWithName(paramString);
    }

    public String getAppleScriptCommand(Command paramCommand, String paramString) {
        switch (paramCommand) {
            default:
                return "";
            case SYSTEM_VOLUME:
        }
        return String.format(getSystemItem(SET_VOLUME_SYSTEM_ITEM_NAME).getCommand(), new Object[]{paramString});
    }

    public String getOsascriptCommand(Command paramCommand) {
        return getOsascriptCommand(paramCommand, null);
    }

    public String getOsascriptCommand(Command paramCommand, String paramString) {
        if (paramString == null) ;
        for (String str = getAppleScriptCommand(paramCommand); ; str = getAppleScriptCommand(paramCommand, paramString)) {
            if (str != null)
                str = str.replaceAll("'", "\\\\\"");
            return String.format("osascript -e '%s'", new Object[]{str});
        }
    }


}