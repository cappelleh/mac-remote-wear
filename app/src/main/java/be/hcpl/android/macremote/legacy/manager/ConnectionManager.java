package be.hcpl.android.macremote.legacy.manager;

import android.content.Context;
import android.os.AsyncTask;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.ByteArrayOutputStream;
import java.util.Properties;

import be.hcpl.android.macremote.MainApplication;
import be.hcpl.android.macremote.common.Command;
import be.hcpl.android.macremote.legacy.event.CommandResponseEvent;
import be.hcpl.android.macremote.legacy.event.ConnectionFailureEvent;
import be.hcpl.android.macremote.legacy.event.ConnectionSuccesfulEvent;
import be.hcpl.android.macremote.legacy.model.AppData;
import be.hcpl.android.macremote.legacy.model.Credentials;

public class ConnectionManager {

    private static final int LONG_TIMEOUT = 10000;
    private static final int SHORT_TIMEOUT = 3000;

    private static ConnectionManager instance;

    private Context ctx;
    private MainApplication app;

    private int port = 22;
    private Session session;

    private ConnectionManager(MainApplication app) {
        this.app = app;
        this.ctx = app.getApplicationContext();
    }

    public static ConnectionManager getInstance(MainApplication app) {
        if (instance == null)
            instance = new ConnectionManager(app);
        return instance;
    }

    public void connect(final Credentials paramCredentials) {

        // network connections should be run in background
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... nothing) {

                JSch localJSch = new JSch();
                try {
                    session = localJSch.getSession(paramCredentials.getUsername(), paramCredentials.getIp(), ConnectionManager.this.port);
                    session.setPassword(paramCredentials.getPassword());
                    Properties localProperties = new Properties();
                    localProperties.put("StrictHostKeyChecking", "no");
                    session.setConfig(localProperties);
                    session.connect(LONG_TIMEOUT);
                    // returning null on success
                    DatabaseManager localDatabaseManager = DatabaseManager.getInstance(ConnectionManager.this.ctx);
                    Credentials localCredentials = localDatabaseManager.addDistinctCredentials(paramCredentials);
                    AppData localAppData = localDatabaseManager.getAppData();
                    localAppData.setCurrentCredentials(localCredentials);
                    localDatabaseManager.updateAppData(localAppData);
                    session.setTimeout(SHORT_TIMEOUT);
                    return null;
                } catch (JSchException localJSchException) {
                    String str = handleMessage(localJSchException.getMessage());
                    return new StringBuilder("Can't connect.\n").append(str).toString();
                }
            }

            @Override
            protected void onPostExecute(String message) {
                if (message == null) {
                    app.getBus().post(new ConnectionSuccesfulEvent());
                } else {
                    app.getBus().post(new ConnectionFailureEvent(message));
                }
            }
        }.execute();

    }

    /**
     * helper to construct err msg
     *
     * @param originalMessage
     * @return
     */
    private String handleMessage(String originalMessage) {

        // TODO use translated messages instead

        String resultMessage = "Check your username and password.";
        if (originalMessage == null)
            return resultMessage;
        if ("timeout: socket is not established".equals(originalMessage))
            resultMessage = "Check your wifi or ip address";
        else if (originalMessage.contains("java.net.ConnectException: failed to connect to"))
            resultMessage = "Check your wifi or ip address";
        else if (originalMessage.contains("java.net.UnknownHostException: Unable to resolve host"))
            resultMessage = "Check your wifi or ip address";
        else
            resultMessage = "Check your wifi and connection details";
        return resultMessage;
    }

    public void disconnect() {
        session.disconnect();
    }

    public void execute(Command paramCommand) {
        execute(paramCommand, null);
    }

    public void execute(final Command paramCommand, String paramString) {
        final String str;
        if (paramString == null)
            str = CommandManager.getInstance(this.ctx).getOsascriptCommand(paramCommand);
        else
            str = CommandManager.getInstance(this.ctx).getOsascriptCommand(paramCommand, paramString);

        // network connections should be run in background
        new AsyncTask<Void, Void, CommandResponseEvent>() {
            @Override
            protected CommandResponseEvent doInBackground(Void... nothing) {

                CommandResponseEvent event = null;

                try {
                    ChannelExec localChannelExec = (ChannelExec) session.openChannel("exec");
                    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
                    localChannelExec.setOutputStream(localByteArrayOutputStream);
                    localChannelExec.setCommand(str);
                    localChannelExec.connect();

                    event = new CommandResponseEvent(paramCommand, localByteArrayOutputStream.toString("UTF-8"));

                    while (localChannelExec.getExitStatus() == -1)
                        try {
                            Thread.sleep(100L);
                        } catch (Exception localException) {
                            System.out.println(localException);
                        }
                    localChannelExec.disconnect();

                    // need to recreate the event here with the data for this to work
                    return new CommandResponseEvent(paramCommand, localByteArrayOutputStream.toString("UTF-8"));

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return event;

            }

            @Override
            protected void onPostExecute(CommandResponseEvent event) {
                if (event != null)
                    app.getBus().post(event);

            }
        }.execute();
    }

    public boolean isConnected() {
        return (session != null) && (session.isConnected());
    }
}