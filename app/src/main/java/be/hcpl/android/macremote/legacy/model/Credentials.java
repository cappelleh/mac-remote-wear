package be.hcpl.android.macremote.legacy.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Credentials
{
  public static final String ID = "id";
  public static final String IP = "ip";
  public static final String PASSWORD = "password";
  public static final String USERNAME = "username";

  @DatabaseField(columnName="id", generatedId=true)
  private int id;

  @DatabaseField(columnName="ip")
  private String ip;

  @DatabaseField(columnName="password")
  private String password;

  @DatabaseField(columnName="username")
  private String username;

  public int getId()
  {
    return this.id;
  }

  public String getIp()
  {
    return this.ip;
  }

  public String getPassword()
  {
    return this.password;
  }

  public String getUsername()
  {
    return this.username;
  }

  public void setId(int paramInt)
  {
    this.id = paramInt;
  }

  public void setIp(String paramString)
  {
    this.ip = paramString;
  }

  public void setPassword(String paramString)
  {
    this.password = paramString;
  }

  public void setUsername(String paramString)
  {
    this.username = paramString;
  }
}