package be.hcpl.android.macremote.wear;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import be.hcpl.android.macremote.R;
import be.hcpl.android.macremote.common.Command;
import be.hcpl.android.macremote.common.Constants;
import be.hcpl.android.macremote.legacy.model.AppCommand;
import be.hcpl.android.macremote.legacy.model.MediaItem;

public class WearActivity extends Activity implements MessageApi.MessageListener {

    private static final long CONNECTION_TIME_OUT_MS = 1000;

    /**
     * status and welcome text
     */
    private TextView mTextView;

    /**
     * logging tag
     */
    private static final String TAG = WearActivity.class.getSimpleName();

    private GoogleApiClient client;

    /**
     * ID form the handheld kept as reference for responses?
     */
//    private String nodeId;

    /**
     * the viewstub reference so we can invalidate it for updates
     */
    private WatchViewStub stub;

    /**
     * a collection of all the apps supported by the configuration found on the device. This list is
     * synced all the time.
     */
//    private String[] allApplications = new String[]{};
    private List<MediaItem> allApplications = new ArrayList<>();

    /**
     * the current index in the list
     */
    private int currentIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(R.id.text);
                mTextView.setText(R.string.app_name);
                // register the listeners
                findViewById(R.id.prev).setOnClickListener(onClickListener);
                findViewById(R.id.play).setOnClickListener(onClickListener);
                findViewById(R.id.next).setOnClickListener(onClickListener);
                findViewById(R.id.info).setOnClickListener(onClickListener);

                // just use regular touch gestures for scrolling through list for now since below
                // impl ain't working
                mTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateIndex();
                    }
                });

//                 send an empty message to get a list of apps (init sync)
//                sendMessage("");
                // using prefixes for commands now instead
                sendMessage(Constants.WEAR_COMMAND_APPLIST);

                // first help info
                // TODO only show once? or let the app control if this should be shown based on usage + settings
                Toast.makeText(getApplication(), R.string.info_toggle_app, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void updateIndex(int desiredIndex) {

        // TODO rewrite this logic

        // go to next item in list
        if (allApplications == null || allApplications.isEmpty())
            return;
        // if we passed a desiredIndex we need to navigate to that position
        if (desiredIndex != Integer.MIN_VALUE) {
            if (desiredIndex >= allApplications.size()) {
                Log.d(TAG, "index to high, move to first item instead");
                desiredIndex = 0;
            }
            currentIndex = desiredIndex;
            mTextView.setText(allApplications.get(currentIndex).getName());
        }
        // if no desired index passed just go to the next item
        else {
            // reset first if needed
            if (currentIndex >= allApplications.size() - 1)
                currentIndex = -1;
            mTextView.setText(allApplications.get(++currentIndex).getName());
        }
    }

    private void updateIndex() {
        updateIndex(Integer.MIN_VALUE);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            // check if there is an app to prefix with
            if (currentIndex >= 0 && allApplications != null && currentIndex <= allApplications.size()) {

                AppCommand appCommand = new AppCommand();
                appCommand.setAppName(allApplications.get(currentIndex).getName());

                // complete command
                switch (view.getId()) {
                    case R.id.prev:
                        appCommand.setAppCommand(Command.PREVIOUS);
                        break;
                    case R.id.play:
                        appCommand.setAppCommand(Command.PLAY_PAUSE);
                        break;
                    case R.id.next:
                        appCommand.setAppCommand(Command.NEXT);
                        break;
                    case R.id.info:
                        appCommand.setAppCommand(Command.CURRENT_TRACK);
                        break;
                    // TODO implement volume and other system controls
                }

                sendMessage(Constants.WEAR_COMMAND_APPCOMMAND + new Gson().toJson(appCommand));

            }
        }
    };

    private GoogleApiClient getGoogleApiClient(Context context) {
        if (client == null)
            client = new GoogleApiClient.Builder(context)
                    .addApi(Wearable.API)
                    .build();
        return client;
    }

    private void sendMessage(final String message) {
        final GoogleApiClient client = getGoogleApiClient(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                NodeApi.GetConnectedNodesResult result =
                        Wearable.NodeApi.getConnectedNodes(client).await();
                List<Node> nodes = result.getNodes();
                if (nodes.size() > 0) {
                    final String nodeId = nodes.get(0).getId();
                    if (nodeId != null) {

                        client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                        Wearable.MessageApi.sendMessage(client, nodeId, message, null);
                        client.disconnect();

                    }
                    client.disconnect();
                }
            }
        }).start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Wearable.MessageApi.addListener(getGoogleApiClient(getApplicationContext()), this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Wearable.MessageApi.removeListener(getGoogleApiClient(getApplicationContext()), this);
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        // only needed for reply
//        nodeId = messageEvent.getSourceNodeId();
        final String message = messageEvent.getPath();
        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                // if the message starts with the magic [list] keyword this is a listing of all
                // available apps known in config
                if (message != null && message.startsWith(Constants.WEAR_COMMAND_APPLIST)) {

                    // use json here for unwrapping the message instead of index based
//                    allApplications = message.replace(Constants.WEAR_COMMAND_APPLIST, "").split(";");
                    allApplications = (List) new Gson().fromJson(message.replace(Constants.WEAR_COMMAND_APPLIST, ""), Constants.MEDIA_ITEM_TYPE);
                    // and reset current index to first item (will be incremented)
                    updateIndex(0);

                }
                // otherwise it's just the name of the current app
                else if (message != null && message.startsWith(Constants.WEAR_COMMAND_APPNAME)) {
                    mTextView.setText(message.replace(Constants.WEAR_COMMAND_APPNAME, ""));
                    // and force an update of the screen
                    stub.invalidate();
                }
                // or a toast message
                else if (message != null && message.startsWith(Constants.WEAR_COMMAND_MESSAGE)) {
                    Toast.makeText(getApplication(), message.replace(Constants.WEAR_COMMAND_MESSAGE, ""), Toast.LENGTH_SHORT).show();
                }
                // no proper command definition, use logging instead
                else {
                    Log.d(TAG, "received bad prefixed message: " + message);
                }
            }

        });
    }
}
