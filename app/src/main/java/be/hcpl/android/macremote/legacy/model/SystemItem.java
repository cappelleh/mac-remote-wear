package be.hcpl.android.macremote.legacy.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class SystemItem
{
  public static final String COMMAND = "command";
  public static final String ICON = "icon";
  public static final String ID = "id";
  public static final String IS_CUSTOM = "is_custom";
  public static final String IS_EDITABLE = "is_editable";
  public static final String IS_REMOVED = "is_removed";
  public static final String NAME = "name";
  public static final String POSITION = "position";

  @SerializedName("command")
  @DatabaseField(columnName="command")
  private String command;

  @SerializedName("icon")
  @DatabaseField(columnName="icon")
  private String icon;

  @DatabaseField(columnName="id", generatedId=true)
  private int id;

  @DatabaseField(columnName="is_custom", defaultValue="false")
  private boolean isCustom;

  @SerializedName("is_editable")
  @DatabaseField(columnName="is_editable", defaultValue="false")
  private boolean isEditable;

  @DatabaseField(columnName="is_removed", defaultValue="false")
  private boolean isRemoved;

  @SerializedName("name")
  @DatabaseField(columnName="name")
  private String name;

  @DatabaseField(columnName="position")
  private int position;

  public String getCommand()
  {
    return this.command;
  }

  public String getIcon()
  {
    return this.icon;
  }

  public int getId()
  {
    return this.id;
  }

  public String getName()
  {
    return this.name;
  }

  public int getPosition()
  {
    return this.position;
  }

  public boolean isCustom()
  {
    return this.isCustom;
  }

  public boolean isEditable()
  {
    return this.isEditable;
  }

  public boolean isRemoved()
  {
    return this.isRemoved;
  }

  public void setCommand(String paramString)
  {
    this.command = paramString;
  }

  public void setCustom(boolean paramBoolean)
  {
    this.isCustom = paramBoolean;
  }

  public void setEditable(boolean paramBoolean)
  {
    this.isEditable = paramBoolean;
  }

  public void setIcon(String paramString)
  {
    this.icon = paramString;
  }

  public void setId(int paramInt)
  {
    this.id = paramInt;
  }

  public void setName(String paramString)
  {
    this.name = paramString;
  }

  public void setPosition(int paramInt)
  {
    this.position = paramInt;
  }

  public void setRemoved(boolean paramBoolean)
  {
    this.isRemoved = paramBoolean;
  }
}