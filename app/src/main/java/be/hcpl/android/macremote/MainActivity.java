package be.hcpl.android.macremote;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import be.hcpl.android.appframework.TemplateFragment;
import be.hcpl.android.appframework.TemplateMainActivity;
import be.hcpl.android.macremote.fragment.AboutFragment;
import be.hcpl.android.macremote.fragment.AppListFragment;
import be.hcpl.android.macremote.fragment.HelpFragment;
import be.hcpl.android.macremote.legacy.ConnectFragment;
import be.hcpl.android.macremote.legacy.ControlsFragment;
import be.hcpl.android.macremote.legacy.event.UpdateAppsEvent;
import be.hcpl.android.macremote.legacy.manager.DatabaseManager;
import be.hcpl.android.macremote.legacy.manager.JsonManager;
import be.hcpl.android.macremote.legacy.model.AppData;
import be.hcpl.android.macremote.legacy.model.MediaItem;
import be.hcpl.android.macremote.legacy.util.TimeUtils;


/**
 * this main activity is only in place to handle the fragment navigation, no beacon related stuff
 * to be found here
 */
public class MainActivity extends TemplateMainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // and title
        setTitle(R.string.app_title);
        //getActionBar().setIcon(getResources().getDrawable(R.drawable.ic_logo));
        // set system bar tint done in style using official L preview impl

        // register for events
        ((MainApplication) getApplication()).getBus().register(this);

        // check if we need to init data here
        DatabaseManager dbManager = DatabaseManager.getInstance(getApplicationContext());
        if (dbManager.getAppData() == null) {
            initDb(dbManager);
        }
    }

    /**
     * helper to initi local app data on first run
     */
    private void initDb(DatabaseManager dbManager) {
        AppData localAppData = new AppData();
        localAppData.setFirstRunTimestamp(TimeUtils.getCurrentTimestamp());
        dbManager.addAppData(localAppData);
        JsonManager localJsonManager = JsonManager.getInstance(getApplicationContext());
        List localList1 = localJsonManager.getSystemItems();
        List localList2 = localJsonManager.getMediaItems();
        dbManager.addSystemItems(localList1);
        dbManager.addMediaItems(localList2);

        // update apps needs to be triggered for new menu content here
        ((MainApplication)getApplication()).getBus().post(new UpdateAppsEvent());
    }

    @Override
    protected void onResume() {
        // always show the about by default
        // TODO create some abstract initial fragment method instead in template
        if( mContentFragment == null )
            mContentFragment = AboutFragment.createInstance();
        super.onResume();

    }

    @Override
    public void onBackPressed() {

        // don't rely on contentFragment since that will be ruined when messing with backstack like
        // we do here for avoiding the connectFragment loop.
        //if (mContentFragment instanceof ConnectFragment) {
        FragmentManager fm = getSupportFragmentManager();
        if( fm.findFragmentById(R.id.container) instanceof ConnectFragment){

            // go 2 entries back if enough available
            if( fm.getBackStackEntryCount() >= 2)
                fm.popBackStackImmediate(fm.getBackStackEntryAt(fm.getBackStackEntryCount()-2).getName(), 1);

            // don't just switch back since this would be adding up to backstack also
            //switchContent(AboutFragment.createInstance());

            // this didn't work for me
            //getSupportFragmentManager().popBackStackImmediate();
            //getSupportFragmentManager().popBackStackImmediate();
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((MainApplication) getApplication()).getBus().unregister(this);
    }

    @Subscribe
    public void onEvent(UpdateAppsEvent event) {
        // just need to refresh menu here
        mNavigationDrawerFragment.getMenuAdapter().getData().clear();
        mNavigationDrawerFragment.getMenuAdapter().getData().addAll(getMenuItems());
        mNavigationDrawerFragment.getMenuAdapter().notifyDataSetChanged();
    }

    public List<TemplateFragment> getMenuItems() {

        // needs database here
        DatabaseManager dbm = DatabaseManager.getInstance(getApplicationContext());

        // construct items
        List<TemplateFragment> fragments = new ArrayList<>();
        for (MediaItem item : dbm.getAllMediaItems()) {
            fragments.add(ControlsFragment.createInstance(item));
        }
        // no more needed
        if (fragments.isEmpty())
            fragments.add(ConnectFragment.createInstance());

        fragments.add(AppListFragment.createInstance());
        fragments.add(HelpFragment.createInstance());
        fragments.add(AboutFragment.createInstance());

        return fragments;
    }


}