package be.hcpl.android.macremote.legacy.manager;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import be.hcpl.android.macremote.MainApplication;
import be.hcpl.android.macremote.legacy.event.UpdateAppsEvent;
import be.hcpl.android.macremote.legacy.model.MediaItem;

public class AppsManagementManager {
    private static AppsManagementManager instance;
    private MainApplication app;
    private Context ctx;
    public MediaItem currentMediaItem;
    public List<Object> sortedList;

    private AppsManagementManager(MainApplication application) {
        this.app = application;
        this.ctx = application.getApplicationContext();
        this.sortedList = new ArrayList();
        List localList1 = DatabaseManager.getInstance(ctx).getNotRemovedMediaItems();
        List localList2 = DatabaseManager.getInstance(ctx).getRemovedMediaItems();
        this.sortedList.addAll(localList1);
        this.sortedList.add(new NotUsedAppsDivider());
        this.sortedList.addAll(localList2);
    }

    private int getDividerPosition(List<Object> paramList) {
        for (int i = 0; i < paramList.size(); i++)
            if ((paramList.get(i) instanceof NotUsedAppsDivider))
                return i;
        return -1;
    }

    public static AppsManagementManager getInstance(MainApplication app) {
        if (instance == null)
            instance = new AppsManagementManager(app);
        return instance;
    }

    public void addNewMediaItem() {
        MediaItem localMediaItem = new MediaItem();
        localMediaItem.setName("My app");
        localMediaItem.setEditable(true);
        localMediaItem.setCustom(true);
        DatabaseManager.getInstance(this.ctx).addMediaItem(localMediaItem);
        this.sortedList.add(0, localMediaItem);
        this.currentMediaItem = localMediaItem;
    }

    public void removeCurrentMediaItem() {
        this.sortedList.remove(this.currentMediaItem);
        DatabaseManager.getInstance(this.ctx).deleteMediaItem(this.currentMediaItem);
    }

    public void saveCurrentMediaItem() {
        DatabaseManager.getInstance(this.ctx).createOrUpdateMediaItem(this.currentMediaItem);
    }

    public void saveMediaItems() {
        int i = getDividerPosition(this.sortedList);
        List localList1 = (List) this.sortedList.subList(0, i);
        List localList2 = (List) this.sortedList.subList(i + 1, this.sortedList.size());
        DatabaseManager localDatabaseManager = DatabaseManager.getInstance(this.ctx);
        for (int j = 0; j < localList1.size(); j++) {
            MediaItem localMediaItem2 = localDatabaseManager.getMediaItemWithId(((MediaItem) localList1.get(j)).getId());
            localMediaItem2.setPosition(j);
            localMediaItem2.setRemoved(false);
            localDatabaseManager.updateMediaItem(localMediaItem2);
        }
        for (int k = 0; k < localList2.size(); k++) {
            MediaItem localMediaItem1 = localDatabaseManager.getMediaItemWithId(((MediaItem) localList2.get(k)).getId());
            localMediaItem1.setPosition(k);
            localMediaItem1.setRemoved(true);
            localDatabaseManager.updateMediaItem(localMediaItem1);
        }
        app.getBus().post(new UpdateAppsEvent());
    }

    public static class NotUsedAppsDivider {
    }
}