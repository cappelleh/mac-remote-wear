package be.hcpl.android.macremote.legacy;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.squareup.otto.Subscribe;

import be.hcpl.android.appframework.TemplateFragment;
import be.hcpl.android.macremote.MainActivity;
import be.hcpl.android.macremote.MainApplication;
import be.hcpl.android.macremote.R;
import be.hcpl.android.macremote.common.Command;
import be.hcpl.android.macremote.legacy.event.CommandResponseEvent;
import be.hcpl.android.macremote.legacy.manager.ConnectionManager;
import be.hcpl.android.macremote.legacy.manager.DatabaseManager;
import be.hcpl.android.macremote.legacy.model.AppData;
import be.hcpl.android.macremote.legacy.model.MediaItem;

public class ControlsFragment extends TemplateFragment {

    private ConnectionManager connectionManager;
    private DatabaseManager dbManager;

    // minimal control views
    private View prevView, playView, nextView, infoView;

    // the media item we're working on
    private MediaItem currentMediaItem = null;

    public static final String KEY_MEDIA_ITEM = "media_item";

    /**
     * ctor, requires a media type (app type) to be created
     *
     * @return
     */
    public static ControlsFragment createInstance(final MediaItem mediaItem) {
        ControlsFragment f = new ControlsFragment();
        Bundle b = new Bundle();
        b.putSerializable(KEY_MEDIA_ITEM, mediaItem);
        f.currentMediaItem = mediaItem;
        return f;
    }

    @Override
    public int getTitleResourceId() {
        // never mind this, we use the dynamic title here
        return R.string.title_control;
    }

    @Override
    public String getDynamicTitle() {
        if( currentMediaItem != null )
            return currentMediaItem.getName();
        else
            return "";
    }

    public MediaItem getCurrentMediaItem() {
        return currentMediaItem;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_MEDIA_ITEM, currentMediaItem);
    }

    /**
     * a single click listener for all controls
     */
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        public void onClick(View paramAnonymousView) {
            ControlsFragment.this.disconnectIfNeeded();
            switch (paramAnonymousView.getId()) {
                default:
                    return;
                case R.id.prev:
                    connectionManager.execute(Command.PREVIOUS);
                    return;
                case R.id.play:
                    connectionManager.execute(Command.PLAY_PAUSE);
                    return;
                case R.id.next:
                    connectionManager.execute(Command.NEXT);
                    return;
                case R.id.info:
                    connectionManager.execute(Command.CURRENT_TRACK);
                    return;
                // TODO implement other controls here
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_control, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        prevView = view.findViewById(R.id.prev);
        playView = view.findViewById(R.id.play);
        nextView = view.findViewById(R.id.next);
        infoView = view.findViewById(R.id.info);

        // set listeners
        prevView.setOnClickListener(onClickListener);
        playView.setOnClickListener(onClickListener);
        nextView.setOnClickListener(onClickListener);
        infoView.setOnClickListener(onClickListener);
    }

    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);

        // restore the state
        Bundle args = getArguments();
        if (args != null && args.containsKey(KEY_MEDIA_ITEM)) {
            currentMediaItem = (MediaItem) args.getSerializable(KEY_MEDIA_ITEM);
        } else if (paramBundle != null && paramBundle.containsKey(KEY_MEDIA_ITEM))
            currentMediaItem = (MediaItem) paramBundle.getSerializable(KEY_MEDIA_ITEM);

        this.dbManager = DatabaseManager.getInstance(getActivity().getApplicationContext());
        this.connectionManager = ConnectionManager.getInstance((MainApplication)getActivity().getApplication());

        ((MainApplication)getActivity().getApplication()).getBus().register(this);

        // update title based on media item
        if (currentMediaItem != null && currentMediaItem.getName() != null)
            getActivity().setTitle(currentMediaItem.getName());
    }

    public void onDestroy() {
        super.onDestroy();
        ((MainApplication)getActivity().getApplication()).getBus().unregister(this);
    }

    @Subscribe
    public void onEvent(CommandResponseEvent paramCommandResponseEvent) {
        // all command responses are retrieved here
        //String str = paramCommandResponseEvent.message.replace("\n", "");
        switch (paramCommandResponseEvent.command) {
            default:
                return;
            /*
            case PLAY_PAUSE:
                // TODO do we need to react here on messages?
                //Toast.makeText(this, str, 1).show();
                return;
            case CURRENT_TRACK:
                // show the current track in app here
                // no longer here, moved to service instead
                // Toast.makeText(getActivity().getApplicationContext(), str, Toast.LENGTH_SHORT).show();
*/
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // hide keyboard here
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        // show connect fragment instead if needed
        disconnectIfNeeded();

        // update app data
        AppData appData = this.dbManager.getAppData();
        appData.setCurrentMediaItem(currentMediaItem);
        this.dbManager.updateAppData(appData);
    }

    private void disconnectIfNeeded() {
        // show connect fragment first if needed
        if (!this.connectionManager.isConnected())
            ((MainActivity)getActivity()).switchContent(ConnectFragment.createInstance(currentMediaItem));
    }

}