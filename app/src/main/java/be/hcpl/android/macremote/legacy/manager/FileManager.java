package be.hcpl.android.macremote.legacy.manager;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

public class FileManager {
    public static final String FILENAME_MEDIA_ITEMS = "default_conf/default_media_items.json";
    public static final String FILENAME_SYSTEM_ITEMS = "default_conf/default_system_items.json";
    private static FileManager instance;
    private Context ctx;

    private FileManager(Context paramContext) {
        this.ctx = paramContext;
    }

    public static FileManager getInstance(Context paramContext) {
        if (instance == null)
            instance = new FileManager(paramContext);
        return instance;
    }

    public String readFileFromAssets(String paramString) {
        String json = null;
        try {
            InputStream is = ctx.getAssets().open(paramString);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return json;
    }
}