package be.hcpl.android.macremote.legacy.util;

public class TimeUtils
{
  public static long diffBetweenTimestamp(String paramString)
  {
    return System.currentTimeMillis() - Long.parseLong(paramString);
  }

  public static String getCurrentTimestamp()
  {
    return Long.valueOf(System.currentTimeMillis()).toString();
  }
}