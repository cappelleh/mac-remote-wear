#Concept

Mac Remote for Wear is a basic app for Mac remote control of iTunes, Keynote, iPhoto, VLC and more
with support for android wear.

This app can be used from phone or tablet without wear also. The android wear device is used as an
extra control available on your watch while the app is responsible for the wifi connection.

Mac Remote for Wear is completely free to use and free of ads.

# Features

* Minimalistic design showing most used controls (forward, backward, start/stop)
* android wear support
* works on any mac as long as it has the remote control option checked in sharing options
* no ads, all free
* android support lib

## dependencies

* gson
* apache commons io
* ormlite
* jsch
* otto event bus

## Resources

* http://mvnrepository.com/
* http://toastdroid.com/2014/08/18/messageapi-simple-conversations-with-android-wear/
* https://github.com/mikepenz/AboutLibraries

# Version History

## 0.4.0

* minimal input validation
* title resume fixes
* minimal layout tweaks
* back navigation loop fixed
* using gson for communication now
* improved stability
* close keyboard in some situations it was left open

## 0.3.0

* support for creating other app controls
* created community and added link to about page
* fixed text color on android 5.0 for autocomplete
* show info response on wearable

## 0.2.0

* showing selected app on wear
* change selected app on wear
* fixed back loop on handheld
* first in app help added
* info button implemented

## 0.1.1

* check

## 0.1.0

* first release with support for iTunes, iPhoto, Spotify, Rdio, Keynote and VLC
* otto event bus implemented
* background task for network connection
* autocomplete fields used for login
* material design
* updated library info
