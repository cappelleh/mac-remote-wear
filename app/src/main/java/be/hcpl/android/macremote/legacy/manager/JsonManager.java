package be.hcpl.android.macremote.legacy.manager;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import be.hcpl.android.macremote.common.Constants;
import be.hcpl.android.macremote.legacy.model.MediaItem;
import be.hcpl.android.macremote.legacy.model.SystemItem;

import java.lang.reflect.Type;
import java.util.List;

public class JsonManager {

    private static JsonManager instance;
    private Context ctx;

    private JsonManager(Context paramContext) {
        this.ctx = paramContext;
    }

    public static JsonManager getInstance(Context paramContext) {
        if (instance == null)
            instance = new JsonManager(paramContext);
        return instance;
    }

    public List<MediaItem> getMediaItems() {
        Gson localGson = new Gson();
        return (List) localGson.fromJson(FileManager.getInstance(this.ctx).readFileFromAssets("default_conf/default_media_items.json"), Constants.MEDIA_ITEM_TYPE);
    }

    public List<SystemItem> getSystemItems() {
        Gson localGson = new Gson();
        return (List) localGson.fromJson(FileManager.getInstance(this.ctx).readFileFromAssets("default_conf/default_system_items.json"), Constants.MEDIA_ITEM_TYPE);
    }
}