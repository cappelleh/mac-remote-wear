package be.hcpl.android.macremote.legacy.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable
public class MediaItem implements Serializable
{
  public static final String FAST_FORWARD = "fast_forward";
  public static final String ID = "id";
  public static final String INFO = "info";
  public static final String IS_CUSTOM = "is_custom";
  public static final String IS_EDITABLE = "is_editable";
  public static final String IS_REMOVED = "is_removed";
  public static final String NAME = "name";
  public static final String NEXT = "next";
  public static final String PLAY_PAUSE = "play_pause";
  public static final String POSITION = "position";
  public static final String PREVIOUS = "previous";
  public static final String REWIND = "rewind";

  @SerializedName("fast_forward")
  @DatabaseField(columnName="fast_forward")
  private String fastForward;

  @DatabaseField(columnName="id", generatedId=true)
  private int id;

  @SerializedName("info")
  @DatabaseField(columnName="info")
  private String info;

  @DatabaseField(columnName="is_custom", defaultValue="false")
  private boolean isCustom;

  @SerializedName("is_editable")
  @DatabaseField(columnName="is_editable", defaultValue="false")
  private boolean isEditable;

  @DatabaseField(columnName="is_removed", defaultValue="false")
  private boolean isRemoved;

  @SerializedName("name")
  @DatabaseField(columnName="name")
  private String name;

  @SerializedName("next")
  @DatabaseField(columnName="next")
  private String next;

  @SerializedName("play_pause")
  @DatabaseField(columnName="play_pause")
  private String playPause;

  @DatabaseField(columnName="position")
  private int position;

  @SerializedName("previous")
  @DatabaseField(columnName="previous")
  private String previous;

  @SerializedName("rewind")
  @DatabaseField(columnName="rewind")
  private String rewind;

  public String getFastForward()
  {
    return this.fastForward;
  }

  public int getId()
  {
    return this.id;
  }

  public String getInfo()
  {
    return this.info;
  }

  public String getName()
  {
    return this.name;
  }

  public String getNext()
  {
    return this.next;
  }

  public String getPlayPause()
  {
    return this.playPause;
  }

  public int getPosition()
  {
    return this.position;
  }

  public String getPrevious()
  {
    return this.previous;
  }

  public String getRewind()
  {
    return this.rewind;
  }

  public boolean isCustom()
  {
    return this.isCustom;
  }

  public boolean isEditable()
  {
    return this.isEditable;
  }

  public boolean isRemoved()
  {
    return this.isRemoved;
  }

  public void setCustom(boolean paramBoolean)
  {
    this.isCustom = paramBoolean;
  }

  public void setEditable(boolean paramBoolean)
  {
    this.isEditable = paramBoolean;
  }

  public void setFastForward(String paramString)
  {
    this.fastForward = paramString;
  }

  public void setId(int paramInt)
  {
    this.id = paramInt;
  }

  public void setInfo(String paramString)
  {
    this.info = paramString;
  }

  public void setName(String paramString)
  {
    this.name = paramString;
  }

  public void setNext(String paramString)
  {
    this.next = paramString;
  }

  public void setPlayPause(String paramString)
  {
    this.playPause = paramString;
  }

  public void setPosition(int paramInt)
  {
    this.position = paramInt;
  }

  public void setPrevious(String paramString)
  {
    this.previous = paramString;
  }

  public void setRemoved(boolean paramBoolean)
  {
    this.isRemoved = paramBoolean;
  }

  public void setRewind(String paramString)
  {
    this.rewind = paramString;
  }
}